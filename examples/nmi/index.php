<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <style>
        .container-fluid {
            padding-top: 40px;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
            <h3>Billing Form</h3>
                <div class="alert alert-info">
                    This example defaults to sandbox mode (i.e. $config->setDemo(true) in the integration.php. Enter a test card no. and then move the cursor off the card input field <br />
                    To test card enrolled use card no: 4111111111111112 <br />
                    To test card not enrolled use card no: 4111111111111111 <br />
                    You can also use this example to validate your api key and secret by switching $config->setDemo(false) in the integration.php and setting your supplied api key and secret<br />
                    You use the browser's console to see debug statements when information in entered into the form (you should turn this off before going live by removing the verbose option in the JS SDK initialization
                </div>
            <form id="billing-form" action="/examples/nmi/nmi.php" method="post">
                <input type="hidden" name="x_transaction_id" value="<?=uniqid();?>" data-threeds="id" />
                <input type="hidden" name="x_amount" value="1" data-threeds="amount" />
                <input type="hidden" name="year" value="" data-threeds="year" />
                <p>
                    <label>Card No.</label>
                    <input type="text" name="ccNum" value="" placeholder="enter credit card no. here"  data-threeds="pan" />
                </p>
                <p>
                    <label>Expiration</label>
                    <input type="text" name="expDate" value="1218" placeholder="e.g. 1218"  data-threeds="month" />
                </p>
                <p>
                    <label>First Name</label>
                    <input type="text" name="firstName" value="" />
                </p>
                <p>
                    <label>Last Name</label>
                    <input type="text" name="lastName" value="" />
                </p>
                <p>
                    <label>Street</label>
                    <input type="text" name="street" value="" />
                </p>
                <button>Submit</button>
            </form>
            </div>
        </div>

    </div><!-- /.container -->
    <script src="https://cdn.3dsintegrator.com/threeds-min-v1.6.0.js"></script>
    <script type="application/javascript">
        var tds = new ThreeDS("billing-form","/examples/nmi/integration.php",{verbose:true,rebill:40});
        tds.init();
    </script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
