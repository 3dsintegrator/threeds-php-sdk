#3DS Integrator

This SDK (3DS Integrator) can be used to add risk based authentication to online shopping carts. The SDK follows the same workflow provided in the mpi documentation. 

## Requirements
* PHP 5.6 or higher
* PHP Curl Extension
* Server should support TLS 1.2 

## Quick start
1. Download the [Latest SDK](https://bitbucket.org/3dsintegrator/threeds-php-sdk/get/1.0.zip) and place the autoload.php and lib folder in the document root
1. Create an integration script with the following code in the document root. You can use composer to install the SDK or use the provided `autoload.php`. The code below uses the `autoload.php`. Be sure to update the script with the appropriate configuration options (see the [configuration section](#configuration) for options)
```php
<?php
include_once 'path/to/sdk/autoload.php';
$config = new \ThreeDS\Integrator\Config('api_key','api_secret');
$config->setDemo(true);
$config->setTimeout(12);

if (isset($_POST['threeds_ajax'])) {
    $config->setReturnResult(true);
}


$api = new \ThreeDS\Integrator\Api\Adapter\Curl();
$payment = new \ThreeDS\Integrator\Request\PaymentRequest($_POST);
$responseHandler = new \ThreeDS\Integrator\Response\VerificationResponse();
$integrator = new \ThreeDS\Integrator\Integrator($config,$api,$payment,$responseHandler);

;?>

<?php echo $integrator->render();?>
```
2. Include the javascript library on the billing page before the closing `</body>`. 
```html
<script src="https://cdn.3dsintegrator.com/threeds-min-v1.6.0.js"></script>
```
3. Setup js library. The first parameter is the id on the billing form, the second parameter is the path to the integration script created in step 1. Note this must be an absolute path
```html
<script >
	var tds = new ThreeDS("myBillingForm","http://example.com/integrator.php");
        tds.init();
</script>
```
4. Tag form inputs with the `data-threeds` attribute. The value for attribute depends on the data the input contains. Use the table below to determine for each input

|      Value    |             Description                                                                                        |                                           Example                                             |  
|---------------|----------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| id            | Unique identifiers for transaction. This should not be more than 20 characters long                                                                             |``` <input type="hidden" name="transactionId" data-threeds="id" value="<?=uniqid();?>" /> ```  |
| pan           | Input containing the credit card no.                                                                           |``` <input type="text" name="ccNum" data-threeds="pan" value="" /> ```                         |
| amount        | Input that has the amount to be charged                                                                        |```<input type="hidden" name="amount" data-threeds="amount" value="" />  ```                   |
| month         | 2 digit expiration month. If expiration is not 1 or 2 digits then you may need to implement a customized forms |```<input type="hidden" name="month" data-threeds="month" value="" />  ```                     |
| year          | 2 digit expiration year. If expiration is not 1 or 2 digits then you may need to implement a customized forms  |```<input type="hidden" name="year" data-threeds="year" value="" />  ```                       |

See the examples folder for code samples

## How It Works
The javascript library passes the info from the specially marked inputs to the integration script. The integration script passes the information to the 3dsintegrator api. User verification is done by both the SDK and JS library with the JS library doing the parts that are done in the browsers while the SDK takes care of the server-side calls


## Configuration 
There are a number of configuration options available to help the sdk fit integration needs

### PHP SDK Config Parameters
On the `ThreeDS\Integrator\Config` class there are a few options available:


|      Method      | Default |                                                                                               Description                                                                                               | Example                                                         |
|:----------------:|:-------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|-----------------------------------------------------------------|
| setDemo          | false   | Toggle sandbox mode on and off                                                                                                                                                                          | $config->setDemo(true)                                          |
| setTimeOut       | 10      | How long to wait for the challenge in the iframe to return results  before moving on with payment processing                                                                                            | $config->setTimeOut(10)                                         |
| setHideForm      | true    | In some instances users are prompted with a challenge. This option hides the challenge form. After the configured timeout the payment will be forwarded to the processing form without 3DS verification | $config->setHideForm(false)                                     |
| setIntegratorUrl |         | This is the url to the integration page that was setup. By default the config tries to construct it                                                                                                     | $config->setIntegratorUrl('http://example.com/integration.php') |


Note that the get methods MUST be implemented. `getValue` can be used to retrieve data from the form post e.g. if the billing form has an input named `expDate` then it can be retrieved using `$this->getValue('expDate');`

For a full example of a custom payment form see the examples/custom folder.
 
 ### ThreeDS.js Config Parameters
 |      Parameter   | Default |                                                                                               Description                                                                      |
 |:----------------:|:-------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
 | allowOverride    | false   | Override data validation                                                                                                                                                       | 
 | appendForm       | true    | Automatically add the 3dsecure response values (eci,cavv,xid) as hidden inputs on the billing form                                                                             | 
 | autoSubmit       | true    | When enabled the library listens for changes on input with the attribute "data-threeds" and does a verification automatically| $config->setHideForm(false)                     |
 | iframeId         |         | This is the id for the iframe used to do the verification                                                                                                                      |
 | verbose          | false   | Output information that is helpful in debugging                                                                                                                                | 
 | eciInputId       | eci     | The "name" for the input used to send the eci response value. Only applicable if appendForm option is set to true                                                              | 
 | cavvInputId      | cavv    | The "name" for the input used to send the cavv response value. Only applicable if appendForm option is set to true                                                             | 
 | xidInputId       | xid     | The "name" for the input used to send the xid response value. Only applicable if appendForm option is set to true                                                              |
 | statusInputId    | status  | The "name" for the input used to send the status response value. Only applicable if appendForm option is set to true                                                              |
 | rebill           | false   | This is the amount that the rebill should be done for if rebill is desired                                                          |
  
  ### ThreeDS.js Init 
  The `init` method in the library is used to quickly get started with ThreeDS.js. The easiest call is 
  ```html
  <script >
  	var tds = new ThreeDS("myBillingForm","http://example.com/integrator.php");
          tds.init();
  </script>
  ```
  You can also pass callbacks to `init` to trigger functionality after a response is received
  ### ThreeDS.js Rebills
  For platforms that support rebills the SDK can be configured to automatically do another verification call after the initial verification has successfully completed.
  ```html
    var tds = new ThreeDS("billing-form","http://example.com/integration.php",{verbose:true,rebill:40});
    tds.init();
  ```
  The response from the rebill will automatically be populated in the form 
  ```html
          <input type="hidden" name="rebill_eci" value="" />
          <input type="hidden" name="rebill_cavv" value="" />
          <input type="hidden" name="rebill_xid" value="" />
 ```
  ### ThreeDS.js Verify Method
  
  The ThreeDS.js library tries to do a lot of the heavy lifting is you use the `init` method. If you want even more control the `verify` method can also be used (instead of the `init` method).
  The signature of the verify method is `verify(amount,cardNumber,month,year,transactionId,succesCallback,failureCallback)`. 
  
  ### ThreeDS.js Handling Response
  The `init` automatically sets callback handlers that will add the 3Dsecure data to the billing form (if `appendForm` is set to true). 
  When the library tries to add the resposne to the form it uses the `eciInputId` ,`cavvInputId`,`xidInputId` options to find inputs
   with those names and populate them with values. If it can't find inputs with those names it will append them to the form. 
   
   As an example you can have empty hidden inputs in your billing form and the library will populate them with the response 
   
   ```html
        <input type="hidden" name="eci" value="" />
        <input type="hidden" name="cavv" value="" />
        <input type="hidden" name="xid" value="" />
   ```
   
   You can also pass callback handlers to init to trigger additional functionality when a response is received.
  
  ```html
    <script >
      var resolve = function(response) {
          console.log("Response Received",response);
      } 
      
      var reject = function(error) {
          console.log("Error Received",error);
      } 
      
      var rebillResolve = function(response) {
          console.log("Rebill Response Received",response);
      } 
    
      var rebillReject = function(error) {
          console.log("Rebill Error Received",error);
      } 
          
    	var tds = new ThreeDS("myBillingForm","http://example.com/integrator.php");
            tds.init(resolve,reject,rebillResolve,rebillReject);
    </script>
    ```
  