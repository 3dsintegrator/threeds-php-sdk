<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Api\Request;


use ThreeDS\Integrator\Request\AbstractPaymentRequest;

class EnrollRequest implements IRequest
{
    /**
     * @var AbstractPaymentRequest
     */
    protected $paymentData;

    public function __construct(AbstractPaymentRequest $paymentData)
    {
        $this->paymentData = $paymentData;
    }

    public function getData()
    {
        return array(
            'pan' => $this->paymentData->getCardNumber(),
            'card_exp_month' => $this->paymentData->getExpirationMonth(),
            'card_exp_year' => $this->paymentData->getExpirationYear()
        );
    }

    public function getEndPoint()
    {
        return '/enrolled-status';
    }

}