<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Api\Request;

use ThreeDS\Integrator\Config;
use ThreeDS\Integrator\Request\AbstractPaymentRequest;
use ThreeDS\Integrator\Request\PaymentRequest;

/**
 * Verification Response verification
 *
 * @package ThreeDS\Integrator\Api\Request
 */
class VerificationRequest implements IRequest
{
    /**
     * @var PaymentRequest
     */
    protected $paymentData;
    /**
     * @var string
     */
    protected $paRes;
    /**
     * @var Config
     */
    protected $config;

    public function __construct(AbstractPaymentRequest $paymentData,$paRes, Config $config)
    {
        $this->paymentData = $paymentData;
        $this->paRes = $paRes;
        $this->config = $config;
    }

    public function getData()
    {
        return array(
            'pares' => $this->paRes,
            'pan' => $this->paymentData->getCardNumber(),
            'card_exp_month' => $this->paymentData->getExpirationMonth(),
            'card_exp_year' => $this->paymentData->getExpirationYear(),
            'amount' => $this->paymentData->getAmount(),
            'transaction_id' => $this->paymentData->getTransactionId(),
            'message_id' => $this->paymentData->getMessageId(),
            'return_url' => $this->config->getIntegratorUrl()
        );
    }

    public function getEndPoint()
    {
        return '/auth-response';
    }

}