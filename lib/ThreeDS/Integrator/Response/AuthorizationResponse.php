<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Response;

class AuthorizationResponse extends AbstractResponse
{
    public function __construct($response)
    {
        $this->rawResponse = $response;
    }
}